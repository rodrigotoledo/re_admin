class RealEstateImage < ActiveRecord::Base
  belongs_to :real_estate
  has_attached_file :image, :styles => { big: "800x800>",
    medium: "219x219>", thumb: "100x100>" },
    default_url: "/images/:style/missing.png"
  before_save :set_master
  after_save :set_real_estate_master

  def set_master
    if self.real_estate.real_estate_image.nil?
      self.master = true
    end
    true
  end

  def set_real_estate_master
    if self.master
      self.real_estate.real_estate_image_id = self.id
      self.real_estate.save(validate: false)
    end
    true
  end
end
