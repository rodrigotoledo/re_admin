require 'validators/url_validator'
class Company < ActiveRecord::Base
  belongs_to :company
  has_many :real_estates
  has_many :users
  has_many :companies
  has_many :company_details
  accepts_nested_attributes_for :company_details, allow_destroy: true, reject_if: proc { |attributes| attributes['value'].blank? || attributes['detail_id'].blank? }

  validates :name, presence: true
  validates :url, :twitter, :facebook, :google_plus, :linkedin, url: true

  has_attached_file :logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }
  has_attached_file :twitter_logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }
  has_attached_file :facebook_logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }
  has_attached_file :google_plus_logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }
  has_attached_file :linkedin_logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }
  has_attached_file :skype_logo, default_url: "/images/:style/missing.png", :styles => { big: "800x800>",
    medium: "219x219>", list: "100x100>" }

  validates_attachment_content_type :logo, :twitter_logo, :facebook_logo, :google_plus_logo, :linkedin_logo, :skype_logo,
    :content_type => ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'],
    :message => 'permitido apenas imagens'

  def master?
    self.company_id.nil?
  end

  def all_users
    User.where("company_id = ? OR master_company_id = ?", self.id, self.id)
  end

  def all_companies
    Company.where(id: self.company_ids+[self.id])
  end

  def all_real_estates
    RealEstate.where(company_id: self.company_ids+[self.id])
  end

  def full_name
    complement_name = self.master? ? "(Sede)" : "(Filial)"
    "#{complement_name} - #{self.name}"
  end

  def master?
    self.company.nil? || self.company_id == self.id
  end
end
