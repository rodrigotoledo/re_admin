class CompaniesController < ApplicationController
  add_breadcrumb :index, :companies_path
  before_filter :permit_to_sys_admin
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  # GET /companies.json
  def index
    @companies = current_company.all_companies
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    add_breadcrumb :show, company_path(@company)
  end

  # GET /companies/new
  def new
    add_breadcrumb :new, new_company_path
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
    add_breadcrumb :edit, edit_company_path(@company)
  end

  # POST /companies
  # POST /companies.json
  def create
    add_breadcrumb :new, new_company_path
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save
        format.html { redirect_to companies_path, notice: 'Informações salvas com sucesso.' }
        format.json { render action: 'show', status: :created, location: @company }
      else
        format.html { render action: 'new' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    add_breadcrumb :edit, edit_company_path(@company)
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to companies_path, notice: 'Informações salvas com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = current_company.all_companies.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      _params = params.require(:company).permit(:name, :url, :address_zipcode,
        :address, :address_number, :address_complement, :address_neighborhood,
        :address_city, :address_state, :address_country, :twitter, :facebook,
        :google_plus, :linkedin, :skype, :url, :facebook_logo,
        :google_plus_logo, :linkedin_logo, :skype_logo, :logo,
        company_details_attributes:[:value, :detail, :detail_id, :id, :_destroy])
      _params.merge({company: current_company})
    end
end
