# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140109103618) do

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "companies", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "address_zipcode"
    t.string   "address"
    t.string   "address_number"
    t.string   "address_complement"
    t.string   "address_neighborhood"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_country"
    t.string   "twitter"
    t.string   "facebook"
    t.string   "google_plus"
    t.string   "linkedin"
    t.string   "twitter_logo_file_name"
    t.string   "twitter_logo_content_type"
    t.integer  "twitter_logo_file_size"
    t.datetime "twitter_logo_updated_at"
    t.string   "facebook_logo_file_name"
    t.string   "facebook_logo_content_type"
    t.integer  "facebook_logo_file_size"
    t.datetime "facebook_logo_updated_at"
    t.string   "google_plus_logo_file_name"
    t.string   "google_plus_logo_content_type"
    t.integer  "google_plus_logo_file_size"
    t.datetime "google_plus_logo_updated_at"
    t.string   "linkedin_logo_file_name"
    t.string   "linkedin_logo_content_type"
    t.integer  "linkedin_logo_file_size"
    t.datetime "linkedin_logo_updated_at"
    t.string   "skype"
    t.string   "skype_logo_file_name"
    t.string   "skype_logo_content_type"
    t.integer  "skype_logo_file_size"
    t.datetime "skype_logo_updated_at"
    t.boolean  "master",                        default: false
    t.integer  "company_id"
    t.string   "url"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  add_index "companies", ["company_id"], name: "index_companies_on_company_id"

  create_table "company_details", force: true do |t|
    t.integer  "company_id"
    t.integer  "detail_id"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "company_details", ["company_id"], name: "index_company_details_on_company_id"
  add_index "company_details", ["detail_id"], name: "index_company_details_on_detail_id"

  create_table "details", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "goal_real_estates", force: true do |t|
    t.integer  "goal_id"
    t.integer  "real_estate_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "goal_real_estates", ["goal_id"], name: "index_goal_real_estates_on_goal_id"
  add_index "goal_real_estates", ["real_estate_id"], name: "index_goal_real_estates_on_real_estate_id"

  create_table "goals", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "real_estate_images", force: true do |t|
    t.integer  "real_estate_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "master"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "real_estate_images", ["real_estate_id"], name: "index_real_estate_images_on_real_estate_id"

  create_table "real_estates", force: true do |t|
    t.integer  "company_id"
    t.integer  "real_estate_image_id"
    t.string   "real_estate_type",                     default: "house"
    t.string   "title"
    t.text     "short_description",        limit: 255
    t.string   "address_zipcode"
    t.string   "address"
    t.string   "address_number"
    t.string   "address_complement"
    t.string   "address_neighborhood"
    t.string   "address_reference"
    t.string   "address_region"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_country"
    t.float    "selling_price"
    t.float    "comission_price"
    t.boolean  "sold"
    t.boolean  "active"
    t.string   "owner_name"
    t.string   "owner_phone"
    t.string   "owner_mobile_phone"
    t.string   "owner_agent_name"
    t.string   "owner_agent_phone"
    t.string   "owner_agent_mobile_phone"
    t.boolean  "gated"
    t.string   "rooms"
    t.string   "bathrooms"
    t.integer  "terrain_area"
    t.integer  "build_area"
    t.boolean  "fireplace"
    t.boolean  "pool"
    t.boolean  "bbq_area"
    t.boolean  "kennel"
    t.boolean  "dependencies"
    t.boolean  "homemade_house"
    t.boolean  "turkish_bath"
    t.boolean  "game_room"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "sold_price"
    t.date     "sold_at"
    t.text     "description"
  end

  create_table "user_details", force: true do |t|
    t.integer  "user_id"
    t.integer  "detail_id"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_details", ["detail_id"], name: "index_user_details_on_detail_id"
  add_index "user_details", ["user_id"], name: "index_user_details_on_user_id"

  create_table "users", force: true do |t|
    t.integer  "company_id"
    t.string   "email",                  default: "",               null: false
    t.string   "encrypted_password",     default: "",               null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "master"
    t.boolean  "sys_admin"
    t.integer  "master_company_id"
    t.string   "phone"
    t.string   "mobile_phone"
    t.string   "authentication_token"
    t.string   "user_type",              default: "administrative"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"

end
