class AddSoldPriceAndSoldAtToRealEstates < ActiveRecord::Migration
  def change
    add_column :real_estates, :sold_price, :float
    add_column :real_estates, :sold_at, :date
  end
end
