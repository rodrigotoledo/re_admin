class CreateGoalRealEstates < ActiveRecord::Migration
  def change
    create_table :goal_real_estates do |t|
      t.references :goal, index: true
      t.references :real_estate, index: true

      t.timestamps
    end
  end
end
