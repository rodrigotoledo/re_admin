class AddSkypeToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :skype
      t.attachment :skype_logo
    end
  end
end
