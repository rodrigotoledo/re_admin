class AddUrlAndLogoToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :url
      t.attachment :logo
    end
  end
end
