class AddMasterAndSysAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :master, :boolean
    add_column :users, :sys_admin, :boolean
  end
end
