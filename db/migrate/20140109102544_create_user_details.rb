class CreateUserDetails < ActiveRecord::Migration
  def change
    create_table :user_details do |t|
      t.references :user, index: true
      t.references :detail, index: true
      t.string :value

      t.timestamps
    end
  end
end
