class CreateRealEstateImages < ActiveRecord::Migration
  def change
    create_table :real_estate_images do |t|
      t.references :real_estate, index: true
      t.attachment :image
      t.boolean :master

      t.timestamps
    end
  end
end
